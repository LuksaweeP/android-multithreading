package edu.sjsu.android.androidmultithreading;

import android.util.SparseArray;

public class ButtonStrategyMapper {
    private SparseArray<ButtonStrategy> mButtonStrategyArray = new SparseArray<>();

    public ButtonStrategyMapper(int[] buttonIds,
                                ButtonStrategy[] buttonStrategys) {
        for (int i = 0; i < buttonIds.length; ++i)
            mButtonStrategyArray.put(buttonIds[i], buttonStrategys[i]);
    }

    public ButtonStrategy getButtonStrategy(int buttonId) {
        return mButtonStrategyArray.get(buttonId);
    }
}
