package edu.sjsu.android.androidmultithreading;

public interface ButtonStrategy {
    void downloadAndDisplayImage(DownloadContext downloadContext);

    void cancelDownload(DownloadContext downloadContext);
}

