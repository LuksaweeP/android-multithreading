package edu.sjsu.android.androidmultithreading;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class ThreadedDownloadsActivity extends Activity {

    protected final static String TAG = ThreadedDownloadsActivity.class.getSimpleName();
    private ButtonStrategyMapper mButtonStrategyMapper;
    protected ButtonStrategy mActiveButtonStrategy = null;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mButtonStrategyMapper = new ButtonStrategyMapper(new int[]{
                                    R.id.runnable_button,
                                    R.id.messages_button,
                                    R.id.async_task_button,
                                    R.id.reset_image_button},
                                    new ButtonStrategy[]{
                                        new DownloadWithRunnable(),
                                        new DownloadWithMessages(),
                                        new DownloadWithAsyncTask(),
                                        new ResetImage()
                                    });

    }

    private DownloadContext makeDownloadContext() {
        Runnable completionCommand =
                new Runnable() {
                    public void run() {
                        // Indicate there's no active ButtonStrategy.
                        mActiveButtonStrategy = null;
                    }
                };

        return new DownloadContext((EditText) findViewById(R.id.mUrlEditText),
                (ImageView) findViewById(R.id.mImageView),
                this,
                completionCommand);
    }

    public void handleButtonClick(View view) {
        DownloadContext downloadContext = makeDownloadContext();

        downloadContext.hideKeyboard();

        if (mActiveButtonStrategy != null) {
            mActiveButtonStrategy.cancelDownload(downloadContext);
        }

        mActiveButtonStrategy = mButtonStrategyMapper.getButtonStrategy(view.getId());

        mActiveButtonStrategy.downloadAndDisplayImage(downloadContext);
    }
}

