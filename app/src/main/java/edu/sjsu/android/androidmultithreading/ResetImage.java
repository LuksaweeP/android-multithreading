package edu.sjsu.android.androidmultithreading;

public class ResetImage implements ButtonStrategy {
    public void downloadAndDisplayImage(DownloadContext downloadContext) {
        downloadContext.showToast("Resetting image to the default image");
        downloadContext.resetImage(R.drawable.default_image);
    }

    /**
     * "Cancel" a download.
     */
    @Override
    public void cancelDownload(DownloadContext downloadContext) {
        // No-op.
    }
}
