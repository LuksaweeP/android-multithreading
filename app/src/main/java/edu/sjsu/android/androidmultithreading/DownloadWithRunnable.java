package edu.sjsu.android.androidmultithreading;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;


public class DownloadWithRunnable implements ButtonStrategy {

    private Thread mThread = null;
    private Context context;

    @Override
    public void downloadAndDisplayImage(final DownloadContext downloadContext) {
        final Runnable downloadRunnable = new Runnable() {
            @Override
            public void run() {
                // Download the image.
                final Bitmap image = downloadContext.downloadImage(downloadContext.getUrlString());

                // Display the downloaded image to the user.
                downloadContext.displayImage(image);
            }
        };

        downloadContext.showToast
                ("downloading via Handlers and Runnable");
        mThread = new Thread(downloadRunnable);
        mThread.start();
    }

    public void cancelDownload(DownloadContext downloadContext) {
        downloadContext.showToast
                ("Canceling DownloadWithRunnable in progress");

        mThread.interrupt();
    }
}

