package edu.sjsu.android.androidmultithreading;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;

import java.lang.ref.WeakReference;

public class DownloadWithMessages implements ButtonStrategy {
    private Thread mThread = null;

    @Override
    public void downloadAndDisplayImage(final DownloadContext downloadContext) {
        final MessageHandler messageHandler = new MessageHandler(downloadContext);

        final Runnable downloadRunnable = new Runnable() {
            @Override
            public void run() {
                Message msg = messageHandler.obtainMessage(MessageHandler.SHOW_TOAST);

                messageHandler.sendMessage(msg);

                final Bitmap image = downloadContext.downloadImage(downloadContext.getUrlString());

                msg = messageHandler.obtainMessage(MessageHandler.DISPLAY_IMAGE, image);

                messageHandler.sendMessage(msg);
            }
        };

        mThread = new Thread(downloadRunnable);
        mThread.start();
    }

    @Override
    public void cancelDownload(DownloadContext downloadContext) {
        downloadContext.showToast
                ("Canceling DownloadWithMessages in progress");

        mThread.interrupt();
    }

    private static class MessageHandler extends Handler {
        public static final int SHOW_TOAST = 1;
        public static final int DISPLAY_IMAGE = 2;
        final private WeakReference<DownloadContext> mContext;
        public MessageHandler(DownloadContext downloadContext) {
            mContext = new WeakReference<DownloadContext>(downloadContext);
        }

        public void handleMessage(Message msg) {
            final DownloadContext context = mContext.get();

            if (context == null)
                return;

            switch (msg.what) {
                case SHOW_TOAST:
                    context.showToast("downloading via Handlers and Messages");
                    break;

                case DISPLAY_IMAGE:
                    context.displayImage((Bitmap) msg.obj);
                    break;
            }
        }
    }
}

