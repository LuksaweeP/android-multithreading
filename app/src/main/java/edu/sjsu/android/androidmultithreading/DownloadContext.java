package edu.sjsu.android.androidmultithreading;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Looper;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;

class DownloadContext {
    private final String TAG = DownloadContext.class.getSimpleName();

    private final WeakReference<EditText> mUrlEditText;

    private final WeakReference<ImageView> mImageView;

    private final WeakReference<Activity> mActivity;

    private final Runnable mCompletionCommand;

    private final String mDefaultURL = "https://www.nicepng.com/png/detail/6-65581_smiley-png-happy-smiley-png.png";

    public DownloadContext(EditText editText, ImageView imageView, Activity activity, Runnable completionCommand) {
        mUrlEditText = new WeakReference<EditText>(editText);
        mImageView = new WeakReference<ImageView>(imageView);
        mActivity = new WeakReference<Activity>(activity);
        mCompletionCommand = completionCommand;
    }

    public void showToast(String toastString) {
        Toast.makeText(mActivity.get(), toastString, Toast.LENGTH_SHORT).show();
    }

    public void displayImage(final Bitmap image)
    {
        if (onUiThread()) {
            if (image == null)
                showToast("image is corrupted," + " please check the requested URL.");
            else {
                mImageView.get().setImageBitmap(image);
                mCompletionCommand.run();
            }
        }
        else {
            mActivity.get().runOnUiThread(new Runnable() {
                public void run() {
                    displayImage(image);
                }});
        }
    }

    public Bitmap downloadImage(String url) {
        if (url.equals(""))
            url = mDefaultURL;
            Log.e(TAG, "Downloading image: " + url);
        try (InputStream istream =
                     (InputStream) new URL(url).getContent()) {
            return BitmapFactory.decodeStream(istream);
        } catch (Exception e) {
            Log.e(TAG, "Error downloading image: " + e);
            e.printStackTrace();
            return null;
        }
    }

    public String getUrlString() {
        return mUrlEditText.get().getText().toString();
    }

    public void hideKeyboard() {
        InputMethodManager mgr = (InputMethodManager) mActivity.get().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(mUrlEditText.get().getWindowToken(), 0);
    }

    public void resetImage(int imageResource) {
        mImageView.get().setImageResource(imageResource);
        mCompletionCommand.run();
    }

    private Boolean onUiThread() {
        return Looper.getMainLooper() == Looper.myLooper();
    }
}

