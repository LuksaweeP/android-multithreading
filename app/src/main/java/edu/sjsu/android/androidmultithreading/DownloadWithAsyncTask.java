package edu.sjsu.android.androidmultithreading;

import android.graphics.Bitmap;
import android.os.AsyncTask;


public class DownloadWithAsyncTask implements ButtonStrategy {
    private DownloadAsyncTask mDownloader = null;

    private static class DownloadAsyncTask
            extends AsyncTask<String, Integer, Bitmap> {
        public final DownloadContext mDownloadContext;

        public DownloadAsyncTask(DownloadContext downloadContext) {
            mDownloadContext = downloadContext;
        }

        protected void onPreExecute() {
            mDownloadContext.showToast("downloading via AsyncTask");
        }

        protected Bitmap doInBackground(String... urls) {
            return mDownloadContext.downloadImage(urls[0]);
        }

        protected void onPostExecute(Bitmap image) {
            mDownloadContext.displayImage(image);
        }
    };

    @Override
    public void downloadAndDisplayImage
    (final DownloadContext downloadContext) {
        mDownloader = new DownloadAsyncTask(downloadContext);

        mDownloader.execute(downloadContext.getUrlString());
    }

    @Override
    public void cancelDownload(DownloadContext downloadContext) {
        downloadContext.showToast
                ("Canceling DownloadWithAsyncTask in progress");

        mDownloader.cancel(true);
    }
}

